/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.inheritance;

/**
 *
 * @author Aritsu
 */
public class Duck extends Animal {
    private int numberOfWings;
    public Duck(String name, String color) {
        super(name,color,2);
        System.out.println("Duck created");
    }
    public void fly() {
        System.out.println("Duck: "+ name +" Fly!!");
    }
    
    @Override
    public void walk() {
        super.walk();
        System.out.println("Duck: " + name + " walk with " + numberOfLegs + " legs.");
        
    }
    
    @Override
    public void speak() {
        super.speak();
        System.out.println("Duck: " + name + " speak-> Gaph Gaph !!");
    }
    
    
}
